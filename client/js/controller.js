project
// =========================================================================
// Menu Controller
// =========================================================================

.controller('MainCtrl', function ( $scope, $http ) {
  console.log("Menu Controller.");
  	$scope.data = {};
  	$scope.pizzas = {};
	$scope.ingredients = {};
	$scope.basket = {
  		pizzas: []
  	};
  	$scope.order = {
  		username: "",
  		phone: ""
  	}

  	$scope.confirmedOrders = [];


	$http.get('http://localhost:8080/ingredients')
		.success(function(data){
			$scope.ingredients = data;
			$http.get('http://localhost:8080/menu')
		  		.success(function(data){
		  			data.forEach(function(item){
		  				item.quantity = 1;
		  				item.iNames = [];
		  				$scope.ingredients.forEach(function(ingredient){
		  					if(item.ingredients.includes(ingredient.id)){
		  						item.iNames.push(ingredient.label);
		  					}
		  					return;
		  				})

		  			});
		  			$scope.pizzas = data;
		  		}).error(function(error){
		  			$scope.data.error= error;
		  		});
		}).error(function(error){
			$scope.data.error= error;
		});

	$scope.addPizzaToCart = function(pizza){
		$scope.basket.pizzas.push(pizza);
		console.log(pizza);
	}

	$scope.removePizzaFromCart = function(pizza){
		$scope.basket.pizzas.splice($scope.basket.pizzas.indexOf(pizza),1);
	}

	$scope.getPrice = function(){
		var price = 0;
		$scope.basket.pizzas.forEach(function(pizza){
			price += pizza.price;
		});
		return price;
	}

	$scope.submitForm = function(isValid) {
	   if (isValid) {
	     console.log($scope.order);

	     var currentOrder = {
	     	order: $scope.basket.pizzas,
	     	extras: [],
	     	orderInfo: $scope.order
	     };

	     $http.post('http://localhost:8080/order', currentOrder)
	     	.success(function(data){
	     		
	     			$http.get('http://localhost:8080/order/'+data.id)
	     		  		.success(function(data){
	     		  			$scope.confirmedOrders.push(data);
	     		  			console.log($scope.confirmedOrders);
	     		  		}).error(function(error){
	     		  			$scope.data.error= error;
	     		  		});

	     	}).error(function(error){
	     		$scope.data.error= error;
	     	});
	   }
   };
});