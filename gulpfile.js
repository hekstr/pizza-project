var gulp = require('gulp');
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var runSequence = require('run-sequence');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify')

gulp.task('build:prod', function(callback) {
  runSequence(
  	'copy-partials', 
  	'copy-templates', 
  	'copy-index', 
  	'copy-css', 
  	'copy-js', 
  	'uglify-js',   	
  	'inject', 
  	// 'minify-html', 
  	callback);
});

gulp.task('build:dev', function(callback) {
  runSequence('copy-partials', 'copy-templates', 'copy-css', 'copy-js', 'inject', callback);
});

gulp.task('copy-partials', function() {
  return gulp.src(['./client/partials/**/*.html'])
    .pipe(gulp.dest('./build/partials/'));
});

gulp.task('copy-index', function() {
  return gulp.src(['./client/index.html'])
    .pipe(gulp.dest('./build/'));
});

gulp.task('copy-templates', function() {
  return gulp.src(['./client/templates/**/*.html'])
    .pipe(gulp.dest('./build/templates/'));
});

gulp.task('copy-css', function() {
	return gulp.src(['./client/css/main.css'])
	    .pipe(gulp.dest('./build/css/'));
});

gulp.task('copy-js', function() {
	return gulp.src(['./client/js/main.js', './client/js/controller.js'])
	    .pipe(concat('app.js'))
	    .pipe(gulp.dest('./build/js'));
});

gulp.task('inject', function() {
  return gulp.src('./build/index.html')
    .pipe(inject(gulp.src(['./build/js/app.js', './build/css/main.css'], {read: false}), {relative: true}))
    .pipe(gulp.dest('./build'));
});

gulp.task('uglify-js', function() {
  return gulp.src('./build/js/*.js')
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest('./build/js'))
});

gulp.task('minify-html', function() {
  return gulp.src('./build/*.html')
    .pipe(htmlmin())
});
